var constants = require("./constants.js");

function Port() {
    this.name = undefined;
    this.data = undefined;
    this.block = undefined;
    this.direction = constants.PORT_DIRECTION.INPUT;
    this.connections = new Array();
}


Port.prototype.addLink = function(link){
    if(this.direction===constants.PORT_DIRECTION.INPUT){
        // Only one input
        if(this.connections.length===0){
            this.connections.push(link);
        }
        
    } else {
        this.connections.push(link);
    }
};

Port.prototype.setupAsOutput = function(block){
    this.direction = constants.PORT_DIRECTION.OUTPUT;
    this.block = block;
};

Port.prototype.setupAsInput = function(block){
    this.direction = constants.PORT_DIRECTION.INPUT;
    this.block = block;
};

Port.prototype.toJson = function() {
    var json = {
        name: this.name
    };
    return json;
};

// Export this object
exports.Port = Port;