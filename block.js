var port = require("./port.js");
var utils = require("./utils.js");

function Block(){
	this.idempotent = true;
	this.deterministic = true;
	this.systemControlled = true;
	
	this.duration = 1000;
	this.outputDataSize = 1000;
	
    this.inputs = new Array();
    this.inputsByName = new Array();
    this.outputs = new Array();
    this.outputsByName = new Array();
    this.workflow = undefined;
    this.id = null;
}


Block.prototype.getInputCount = function() {
    return this.inputs.length;
};

Block.prototype.addInput = function(port) {
    port.setuptAsInput(this);
    this.inputs.push(port);
    this.inputsByName[port.name] = port;
};

Block.prototype.createInput = function(portName) {
    var p = new port.Port();
    p.name = portName;
    p.setupAsInput(this);
    this.inputs.push(p);
    this.inputsByName[p.name] = p;
};

Block.prototype.getOutputCount = function() {
    return this.outputs.length;
};

Block.prototype.addOutput = function(port) {
    port.setupAsOutput();
    this.outputs.push(port);
    this.outputsByName[port.name] = port;
};

Block.prototype.createOutput = function(portName) {
    var p = new port.Port();
    p.name = portName;
    p.setupAsOutput(this);
    this.outputs.push(p);
    this.outputsByName[p.name] = p;
};

Block.prototype.getOutput = function(name){
    return this.outputsByName[name];
};

Block.prototype.getInput = function(name){
    return this.inputsByName[name];
};

Block.prototype.listDownstreamBlocks = function(){
    var downstreamBlocks = new Array();
    var outputPort;
    for(var i=0;i<this.outputs.length;i++){
        outputPort = this.outputs[i];
        for(var j=0;j<outputPort.connections.length;j++){
            downstreamBlocks.push(outputPort.connections[j].destinationPort.block);
        }
    }
    return downstreamBlocks;
};

Block.prototype.buildUpstreamCost = function(retentionPolicy, costArray){
    // Add the correct calculation for this block
    if(retentionPolicy[this.id]){
        if(retentionPolicy[this.id]==="R"){
            if(!utils.arrayContainsBlockId(costArray, this.id)){
                costArray.push({
                    id: this.id,
                    regenerateDuration: this.duration,
                    storageSize: 0
                });      
            }      
            
            // Work backwards
            for(var i=0;i<this.inputs.length;i++){
                var input = this.inputs[i];
                
                // Anything connected?
                if(input.connections.length>0){
                    if(input.connections[0].sourcePort && input.connections[0].sourcePort.block){
                        var inputBlock = input.connections[0].sourcePort.block;
                        
                        // Only navigate up regenerating blocks
                        if(retentionPolicy[inputBlock.id]==="R"){
                            inputBlock.buildUpstreamCost(retentionPolicy, costArray);
                        }
                    }
                }
            }            
        } else {
            // Just push storage cost
            if(!utils.arrayContainsBlockId(costArray, this.id)){
                costArray.push({
                    id: this.id,
                    regenerateDuration: 0,
                    storageSize: this.outputDataSize
                });   
            }         
        }
    }

};

Block.prototype.toJson = function() {
    var json = {
        id: this.id,
        duration: this.duration,
        outputDataSize: this.outputDataSize
    };
    return json;
};

Block.prototype.calculateExecutionCost = function(executionEnvironment){
	
};

exports.newBlock = function() {
    return new Block();
};

exports.Block = Block;

