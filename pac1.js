var m = require("./model.js");
var b = require("./block.js");
var u = require("./utils.js");
var fs = require("fs");

exports.runExample = function(){
	var workflow = new m.Workflow();
	
	// Create the blocks
	var b1 = u.createSourceBlock("fileReference");
	b1.duration = 0.003; // 10 seconds
	b1.outputDataSize = 0.000001;	// 1KB
		
	var b2 = u.createType1Block("downloadReferences");
	b2.duration = (5.0 / 60.0);		// 5 mins
	b2.outputDataSize = 0.735;		// 735MB
	
	
	var b3 = u.createType1Block("extractHeaders");
	b3.duration = (1.0 / 60.0);		// 1 minute
	b3.outputDataSize = 0.000002;	//2 KB
	
	var b4 = u.createType1Block("binToCsv");
	b4.duration = (10.0 / 60.0);	// 10 mins
	b4.outputDataSize = 3.2;		// 3.2GB
	
	var b5 = u.createType1Block("pac1");
	b5.duration = (30.0 / 60.0);	// 30 mins
	b5.outputDataSize = 0.001;		// 1MB
	
	var b6 = u.createType2Block("rename");
	b6.duration = 0.2 / 60.0;		// 0.2 minutes
	b6.outputDataSize = 0.001;		// 1MB
	
	var b7 = u.createType3Block("exportFiles");
	b7.duration = 0.2 / 60.0;		// 0.2 minutes
	b7.outputDataSize = 0.000002;	// 2KB
	
	var b8 = u.createType4Block("linkFiles");
	b8.duration = 0.2 / 60.0;		// 0.2 minutes
	b8.outputDataSize = 0.000002;	// 2KB
	
	var b9 = u.createType2Block("attachMetadata");
	b9.duration = 0.2 / 60.0;		// 0.2 minutes
	b9.outputDataSize = 0.000002;
	
	workflow.addBlock(b1);
	workflow.addBlock(b2);
	workflow.addBlock(b3);
	workflow.addBlock(b4);
	workflow.addBlock(b5);
	workflow.addBlock(b6);
	workflow.addBlock(b7);
	workflow.addBlock(b8);
	workflow.addBlock(b9);
	
	workflow.addConnection(b1, "output-data", b2, "input-data");
	workflow.addConnection(b1, "output-data", b6, "input-2");
	workflow.addConnection(b2, "output-data", b3, "input-data");
	workflow.addConnection(b2, "output-data", b4, "input-data");
	workflow.addConnection(b4, "output-data", b5, "input-data");
	workflow.addConnection(b5, "output-data", b6, "input-1");
	workflow.addConnection(b6, "output-data", b7, "input-data");
	workflow.addConnection(b1, "output-data", b8, "input-2");
	workflow.addConnection(b7, "output-2", b8, "input-1");
	workflow.addConnection(b3, "output-data", b9, "input-1");
	workflow.addConnection(b7, "output-2", b9, "input-2");
	
	// Define which blocks are non re-runnable
	b1.systemControlled = false;
	b2.systemControlled = false;
	b8.idempotent = false;
	
	// Set up data and times

	var environment = new m.ExecutionEnvironemnt(0.01, 0.252);
	environment.storageDuration = 120;
	
	var policyArray = m.buildRetentionPolicyList(workflow);
	var rawData = m.calculatePlanData(workflow, environment, policyArray);
	
	u.resultSetToCSV(rawData);
	
	// Run this over 10 years
	u.runModel(workflow, environment, 120, "../data/pac1_10year_projection.csv");
	
	/*
	var environment = new m.ExecutionEnvironemnt(0.01, 0.25);
	environment.costModel = false;
	
	u.runModel(workflow, environment, 36, "../data/pac1.csv");
	*/
	//var dotFile = u.generateDotFile(workflow);
	//console.log(dotFile);	
};
