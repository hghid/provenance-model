var m = require("./model.js");
var b = require("./block.js");
var u = require("./utils.js");

exports.runExample = function(){
	var workflow = new m.Workflow();
	
	
	var block1 = u.createType1Block("B1");
	var block2 = u.createType1Block("B2");
	var block3 = u.createType1Block("B3");
	
	
	block1.duration = 3600;
	block1.outputDataSize = 1000000;
	
	//block2.systemControlled = false;
	block3.duration = 7200;
	block3.outputDataSize = 100;
	
	workflow.addBlock(block1);
	workflow.addBlock(block2);
	workflow.addBlock(block3);
	
	workflow.addConnection(block1, "output-data", block2, "input-data");
	workflow.addConnection(block2, "output-data", block3, "input-data");
	
	var rawData = m.calculatePlanData(workflow);
	for(var i=0;i<rawData.length;i++){
		console.log(i + ": " + JSON.stringify(rawData[i]));
	}
	
};


/*
var json = workflow.toJson();
console.log("Some JSON: " + JSON.stringify(json, null,  3));

var retentionPlan = {
	B1: "K",
	B2: "K",
	B3: "R"
};

var env = new m.ExecutionEnvironemnt();
var costArray = new Array();
block3.buildUpstreamCost(retentionPlan, costArray);
for(var i=0;i<costArray.length;i++){
	console.log(i + ": " + JSON.stringify(costArray[i]));
}

var cost = m.calculateCost(workflow, env, retentionPlan);
*/

/*
var policyArray = m.buildRetentionPolicyList(workflow);
for(var i=0;i<policyArray.length;i++){
	console.log("Policy: " + i + ": " + JSON.stringify(policyArray[i]));
	
	var storageTotal = 0;
	var computeTotal = 0;
	
	for(var j=0;j<workflow.blocks.length;j++){
		var costArray = new Array();
		workflow.blocks[j].buildUpstreamCost(policyArray[i], costArray);
		console.log("Cost for block: " + workflow.blocks[j].id);
		for(var k=0;k<costArray.length;k++){
			console.log(k + ": " + JSON.stringify(costArray[k]));
			
			storageTotal+=costArray[k].storageSize;
			computeTotal+=costArray[k].regenerateDuration;
		}
	
	}
	console.log("----------------------");
	console.log("Storage total: " + storageTotal);
	console.log("Compute total: " + computeTotal);
	console.log("----------------------");	
	console.log("===========================================");
	console.log("");
}
*/
