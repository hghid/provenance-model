
var workflow = require("./workflow.js");
var block = require("./block.js");
var utils = require("./utils.js");

function ExecutionEnvironment(storageCost, computeCost){
	this._storageCost = storageCost;			// GB Month
	this._computeCost = computeCost;			// Compute hour
	this.storageDuration = 12;					// Months of storage
	this.costModel = 0;
	this.monthlyDrop = 0.016;
	
	this.awsModel = new Array();
	this.awsModel.push([1, 0.15]);
	this.awsModel.push([32, 0.15]);
	this.awsModel.push([56, 0.14]);
	this.awsModel.push([71, 0.125]);
	this.awsModel.push([81, 0.095]);
	this.awsModel.push([96, 0.085]);
	this.awsModel.push([98, 0.03]);
}

ExecutionEnvironment.prototype.getStorageCost = function(dataSize){
	var totalCost = 0;
	var cost;
	
	if(this.costModel==2){
		// Run a cost model using historical prices as a lookup table
	 	totalCost = 0;
		cost;
		for(var i=0;i<this.storageDuration;i++){
			cost = this.getAWSStorageCostForMonth(i);
			totalCost+=(cost * dataSize);
		}
		return totalCost;
		
	} else if (this.costModel==1){
		// Run a cost model using a month-on-month percentage droi
		totalCost = 0;
		cost = this._storageCost;
		
		for(var i=0;i<this.storageDuration;i++){
			totalCost+=(cost * dataSize);
			cost = cost * (1 - this.monthlyDrop);
		}
		return totalCost;		
	
	} else {	
		var flatCost = dataSize * this._storageCost * this.storageDuration;
		return flatCost;
	}
};

ExecutionEnvironment.prototype.getAWSStorageCostForMonth = function(month){
	var cost = 0;
	var costSet = false;
	for(var i=0;i<this.awsModel.length - 1;i++){
		var m1 = this.awsModel[i][0];
		var m2 = this.awsModel[i + 1][0];
		if(month>=m1 && month<m2){
			cost = this.awsModel[i][1];
			costSet = true;
		}
	}	
	
	// > end
	if(!costSet){
		cost=this.awsModel[this.awsModel.length-1][1];
	}
	return cost;
};

ExecutionEnvironment.prototype.getComputeCost = function(){
	return this._computeCost;
};

function createPolicy(policyLength,counter){
	var num = counter;
	var policy = new Array();
	for(var i=0;i<policyLength;i++){
		policy[i] = 0;
	}
	
	var c = 0;
	while (num > 0) {
		if (num % 2 === 0) {
			policy[c] = 0;
			num = Math.floor(num / 2);
		} else {
			policy[c] = 1;
			num = Math.floor(num / 2);
		}
		c++;
	} 
	 return policy;
};

exports.createPolicy = createPolicy;
exports.Workflow = workflow.Workflow;
exports.ExecutionEnvironemnt = ExecutionEnvironment;

function buildRetentionPolicyList(workflow){
	var policyArray = new Array();
	
	// First get a list of the constraints
	var searchList = new Array();
	var fixedList = new Array();
	
	for(var i=0;i<workflow.blocks.length;i++){
		var block = workflow.blocks[i];
		
		if(block.idempotent===true && block.deterministic===true && block.systemControlled===true){
			searchList.push(block);
		} else {
			fixedList.push(block);
		}
	}
	
	// Now work out all of the combinations
	var combinations = Math.pow(2, searchList.length);
	var bits = searchList.length;
	
	for(var i = 0;i<combinations;i++){
		var policyBits = createPolicy(bits, i);
		var policy = {};
		
		// Add the fixed parts
		for(var j=0;j<fixedList.length;j++){
			policy[fixedList[j].id] = "K";
		}
		
		// Add the variable parts
		for(var j=0;j<searchList.length;j++){
			if(policyBits[j]===1){
				policy[searchList[j].id] = "K";
			} else {
				policy[searchList[j].id] = "R";
			}
		}
		policyArray.push(policy);
	}
	return policyArray;	
};
exports.buildRetentionPolicyList = buildRetentionPolicyList;

function calculatePlanData(workflow, environment, pArray){
	var policyArray;
	if(pArray){
		policyArray = pArray;
	} else {
		policyArray = buildRetentionPolicyList(workflow);
	}
	
	var resultsArray = new Array();
	
	for(var i=0;i<policyArray.length;i++){
		var result = {
			policy: policyArray[i]
		};
		
		result.policyText = utils.policyToText(policyArray[i]);
		var blockResults = new Array();
		
		var storageTotal = 0;
		var computeTotal = 0;
		
		for(var j=0;j<workflow.blocks.length;j++){
			var costArray = new Array();
			workflow.blocks[j].buildUpstreamCost(policyArray[i], costArray);
			blockResults.push(costArray);
			for(var k=0;k<costArray.length;k++){				
				storageTotal+=costArray[k].storageSize;
				computeTotal+=costArray[k].regenerateDuration;
			}			
		}

		result.blocks = blockResults;
		result.totals = {
			storageTotal: storageTotal,
			computeTotal: computeTotal
		};
		
		// If an environment is present, push in the cost data
		if(environment){
			result.costs = {
				storageCost: environment.getStorageCost(storageTotal),
				computeCost: computeTotal * environment.getComputeCost()
			};
		}
		resultsArray.push(result);
	}
	
	return resultsArray;
};

exports.calculatePlanData = calculatePlanData;