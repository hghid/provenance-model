var block = require("./block.js");
var model = require("./model.js");
var fs = require("fs");

exports.arrayContainsBlockId = function(array, blockId){
	for(var i=0;i<array.length;i++){
		var value = array[i];
		if(value.id && value.id==blockId){
			return true;
		}
	}
	return false;
};


function policyToText(policy){
	var policyText = "";
	
	for(var key in policy){
	  policyText+=policy[key];
	}	
	return policyText;
};
exports.policyToText = policyToText;

/** Create a single input-single output block */
function createType1Block(id){
	var b = block.newBlock();
	b.id = id;
	b.createInput("input-data");
	b.createOutput("output-data");
	return b;
};
exports.createType1Block = createType1Block;

/** Create a two-input single output block */
function createType2Block(id){
	var b = block.newBlock();
	b.id = id;
	b.createInput("input-1");
	b.createInput("input-2");
	b.createOutput("output-data");
	return b;
};
exports.createType2Block = createType2Block;

/** Createa one input two output block */
function createType3Block(id){
	var b = block.newBlock();
	b.id = id;
	b.createInput("input-data");
	b.createOutput("output-1");
	b.createOutput("output-2");
	return b;
};
exports.createType3Block = createType3Block;

/** Create a two input block */
function createType4Block(id){
	var b = block.newBlock();
	b.id = id;
	b.createInput("input-1");
	b.createInput("input-2");
	return b;
}; 
exports.createType4Block = createType4Block;
 
/** Create a single output zero input download block */
function createSourceBlock(id){
	var b = block.newBlock();
	b.id = id;
	b.createOutput("output-data");
	return b;
};
exports.createSourceBlock = createSourceBlock;

function resultSetToCSV(rawData){
	
	// Write CSV to console
	if(rawData[0].costs){
		 console.log("ID,Strategy,Storage,Compute,StorageCOST,ComputeCOST,TotalCOST");
	} else {
		console.log("ID,Strategy,Storage,Compute");
	}
	
	for(var i=0;i<rawData.length;i++){
		if(rawData[i].costs){
			var text = "$C_{" + i + "}$,";
			text+=rawData[i].policyText + ",";
			text+=rawData[i].totals.storageTotal.toFixed(3) + ",";
			text+=rawData[i].totals.computeTotal.toFixed(3) + ",";
			text+=rawData[i].costs.storageCost.toFixed(3) + ",";
			text+=rawData[i].costs.computeCost.toFixed(3) + ",";
			text+= (rawData[i].costs.storageCost + rawData[i].costs.computeCost).toFixed(3);
			//console.log(i + "," + rawData[i].policyText + "," + rawData[i].totals.storageTotal + "," + rawData[i].totals.computeTotal + "," + rawData[i].costs.storageCost + "," + rawData[i].costs.computeCost + "," + (rawData[i].costs.storageCost + rawData[i].costs.computeCost));
			console.log(text);
		} else {
			console.log(i + "," + rawData[i].policyText + "," + rawData[i].totals.storageTotal + "," + rawData[i].totals.computeTotal);
		}
	}
	return rawData;
	
};
exports.resultSetToCSV = resultSetToCSV;

exports.runModel = function(workflow, environment, duration, fileName, policyArray){
	var costArray = new Array();
	// Work through from 1 - 120 months
	if(!policyArray){
		policyArray = model.buildRetentionPolicyList(workflow);
	}

	var fileContent = "";
	
	// Print the top csv line
	var headingLine = "Months";
	for(var i=0;i<policyArray.length;i++){
		headingLine+="," + policyToText(policyArray[i]);
	}
	fileContent+=headingLine;
	fileContent+="\n";
	
	for(var months = 1; months<=duration;months++){
		environment.storageDuration = months;

		var rawData = model.calculatePlanData(workflow, environment, policyArray);
		var row = "" + months;
		var rowArray = new Array();
		rowArray.push(months);
		for(var i=0;i<rawData.length;i++){
			row+= "," + ((rawData[i].costs.storageCost + rawData[i].costs.computeCost).toFixed(5));
			rowArray.push(rawData[i].costs.storageCost + rawData[i].costs.computeCost);
		}
		costArray.push(rowArray);
		fileContent+=row;
		fileContent+="\n";
			
	}

	if(fileName){
		console.log("Writing file: " + fileName);
		fs.writeFile(fileName, fileContent, function(err){
			if(err){
				console.log(err);
			}
		});	
		
	} else {
		console.log("Not writing file");
	}
	resultSetToCSV(rawData);
	return costArray;
};

exports.createExample1Workflow = function(){
	var workflow = new model.Workflow();

	var block1 = createSourceBlock("B1");
	var block2 = createType1Block("B2");
	var block3 = createType1Block("B3");
	
	block1.duration = (2.0 / 60.0);
	
	block1.outputDataSize = 0.7;
	block1.systemControlled = false;
	
	block2.duration = (30.0 / 60.0);
	block2.outputDataSize = block1.outputDataSize * 0.2; //1.5;
	
	//block2.systemControlled = false;
	block3.duration = (10.0 / 60.0);
	block3.outputDataSize = 0.001;
	
	workflow.addBlock(block1);
	workflow.addBlock(block2);
	workflow.addBlock(block3);
	
	workflow.addConnection(block1, "output-data", block2, "input-data");
	workflow.addConnection(block2, "output-data", block3, "input-data");	
	return workflow;
};

function generateDotFile(dataflow) {
    var dotFile = "digraph G{\n";
    
    // Do the blocks
    for(var i=0;i<dataflow.blocks.length;i++){
        var block = dataflow.blocks[i];
        if(block.inputs.length==0){
            // Plain source
            dotFile+=block.id + ' [shape="trapezium",label="' + block.id + '"]\n';
            
        } else if(block.outputs.length==0){
            // Plain output
            dotFile+=block.id + ' [shape="invtrapezium",label="' + block.id + '"]\n';
            
        } else {
            dotFile+=block.id + ' [shape="box",label="' + block.id + '"]\n';
        }
        
        
    }
    
    
    // Do the links
    for(var i=0;i<dataflow.blocks.length;i++){
        var block = dataflow.blocks[i];
        for(var j=0;j<block.outputs.length;j++){
            var port = block.outputs[j];
            for(var k=0;k<port.connections.length;k++){
                var link = port.connections[k];
                dotFile+=link.sourcePort.block.id + '->' + link.destinationPort.block.id + '\n';
            }
        }
    }
    
    dotFile+="}";
    return dotFile;
};
exports.generateDotFile = generateDotFile;