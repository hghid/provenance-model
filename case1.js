var m = require("./model.js");
/* This file generates data for case1:
	Simple three block workflow
	


*/

var b = require("./block.js");
var u = require("./utils.js");
var fs = require("fs");

exports.runExample = function(){
	var workflow = new m.Workflow();

	var block1 = u.createSourceBlock("B1");
	var block2 = u.createType1Block("B2");
	var block3 = u.createType1Block("B3");
	
	block1.duration = (2.0 / 60.0);
	
	block1.outputDataSize = 0.05;
	block1.systemControlled = false;
	
	block2.duration = (5.0 / 60.0);
	block2.outputDataSize = 0.1;
	
	//block2.systemControlled = false;
	block3.duration = (5.0 / 60.0);
	block3.outputDataSize = 0.001;
	
	workflow.addBlock(block1);
	workflow.addBlock(block2);
	workflow.addBlock(block3);
	
	workflow.addConnection(block1, "output-data", block2, "input-data");
	workflow.addConnection(block2, "output-data", block3, "input-data");
	
	var environment = new m.ExecutionEnvironemnt(0.03, 0.25);
	environment.costModel = false;
	
	u.runModel(workflow, environment, 36, "../data/case1a.csv");
	u.runModel(workflow, environment, 120, "../data/case1b.csv");
	
	environment.costModel = 2;
	u.runModel(workflow, environment, 36, "../data/case1c.csv");
	u.runModel(workflow, environment, 120, "../data/case1d.csv");
	
	environment.costModel = 1;
	u.runModel(workflow, environment, 36, "../data/case1e.csv");
	u.runModel(workflow, environment, 120, "../data/case1f.csv");
};
