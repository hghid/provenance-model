function Link(){
    this.sourcePort = undefined;
    this.destinationPort = undefined;
}

Link.prototype.toJson = function(){
    var linkJson = {};
    if(this.sourcePort && this.sourcePort.block){
        linkJson.sourceBlock = this.sourcePort.block.id;
        linkJson.sourcePort = this.sourcePort.name;
    }
    
    if(this.destinationPort && this.destinationPort.block){
        linkJson.destinationBlock = this.destinationPort.block.id;
        linkJson.destinationPort = this.destinationPort.name;
    }
    return linkJson;
};

// Export this object
exports.newLink = function(){
    return new Link();
};

exports.Link = Link;