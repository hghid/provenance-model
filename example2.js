var m = require("./model.js");
var b = require("./block.js");
var u = require("./utils.js");
var fs = require("fs");

exports.runExample = function(){
	var workflow = u.createExample1Workflow();
	
	var environment = new m.ExecutionEnvironemnt(0.03, 0.25);
	environment.costModel = false;
	
	u.runModel(workflow, environment, 36, "../data/example1.csv");
	
};
