var m = require("./model.js");
var b = require("./block.js");
var u = require("./utils.js");
var fs = require("fs");

exports.runExample = function(){
	var workflow = new m.Workflow();

	var block1 = u.createSourceBlock("B1");
	var block2 = u.createType1Block("B2");
	var block3 = u.createType1Block("B3");
	
	block1.duration = (2.0 / 60.0);
	
	block1.outputDataSize = 0.05;
	block1.systemControlled = false;
	
	block2.duration = (5.0 / 60.0);
	block2.outputDataSize = 0.1;
	
	//block2.systemControlled = false;
	block3.duration = (5.0 / 60.0);
	block3.outputDataSize = 0.001;
	
	workflow.addBlock(block1);
	workflow.addBlock(block2);
	workflow.addBlock(block3);
	
	workflow.addConnection(block1, "output-data", block2, "input-data");
	workflow.addConnection(block2, "output-data", block3, "input-data");
	
	var environment = new m.ExecutionEnvironemnt(0.03, 0.25);
	environment.costModel = 0;
	
	var policies = m.buildRetentionPolicyList(workflow);
	var reducedPolicies = new Array();
	reducedPolicies.push(policies[0]);
	
	var allKeepPolicies = new Array();
	allKeepPolicies.push(policies[3]);
	
	environment._computeCost = 0.104;
	var m0 = u.runModel(workflow, environment, 120, null, allKeepPolicies);
	var m1 = u.runModel(workflow, environment, 120, null, reducedPolicies);
	
	environment._computeCost = 0.252;
	var m2 = u.runModel(workflow, environment, 120, null, reducedPolicies);
	
	environment._computeCost = 0.504;
	var m3 = u.runModel(workflow, environment, 120, null, reducedPolicies);
	
	
	var fileData = "Month,KKK,M1,M2,M3 \n";
	
	for(var i=0;i<m1.length;i++){
		fileData+=(m1[i][0] + "," + m0[i][1].toFixed(5) + "," + m1[i][1].toFixed(5) + "," + m2[i][1].toFixed(5) + "," + m3[i][1].toFixed(5) + "\n");
	}
	
	fs.writeFile("../data/case1machines.csv", fileData, function(err){
		if(err){
			console.log(err);
		}
	});		
	console.log("Done");
	
	var dotFile = u.generateDotFile(workflow);
	console.log(dotFile);
};