var m = require("./model.js");
var b = require("./block.js");
var u = require("./utils.js");

exports.runExample = function(){
	var workflow = u.createExample1Workflow();
	
	var environment = new m.ExecutionEnvironemnt(0.1, 0.1);
	environment.storageDuration = 120;
	
	var policyArray = m.buildRetentionPolicyList(workflow);
	var rawData = m.calculatePlanData(workflow, environment, policyArray);
	for(var i=0;i<rawData.length;i++){
		console.log(i + ": " + JSON.stringify(rawData[i], null, 3));
	}
	
	u.resultSetToCSV(rawData);
};
