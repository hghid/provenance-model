var block = require("./block.js");
var links = require("./link.js");

var idCounter = 0;

function Workflow(){
	this.blocks = new Array();
}

/** Add a block to this data flow, or create a new empty block */
Workflow.prototype.addBlock = function(b) {
    if (b) {
        this.blocks.push(b);
        b.workflow = this;
        if(!b.id){
            b.id = ++idCounter;
        }
        return b;
    } else {
        var b = block.newBlock();
        this.blocks.push(b);
        b.workflow = this;
        b.id = ++idCounter;
        return b;
    }
};

Workflow.prototype.addConnection = function(sourceBlock, sourcePortName, destinationBlock, destinationPortName){
    var sb;
    
    var db;
    var dp;
    
    // Find the source for the connection
    if(sourceBlock instanceof block.Block){
        sb = sourceBlock;
    } else {
        sb = this.getBlockById(sourceBlock);
    }
    var sp = sb.getOutput(sourcePortName);
    
    // Find the destination for the connection
    if(destinationBlock instanceof block.Block){
        db = destinationBlock;
    } else {
        db = this.getBlockById(destinationBlock);
    }
    var dp = db.getInput(destinationPortName);
    
    if(sb && sp && db && dp){
        var link = links.newLink();
        link.sourcePort = sp;
        link.destinationPort = dp;
        
        sp.addLink(link);
        dp.addLink(link);
    }
};

Workflow.prototype.getBlockById = function(id){
    for(var i=0;i<this.blocks.length;i++){
        if(id===this.blocks[i].id){
            return this.blocks[i];
        }
    }
    return null;
};

/** Create a json representation of this dataflow */
Workflow.prototype.toJson = function() {
    var blocksJson = new Array();
    for (var i = 0; i < this.blocks.length; i++) {
        blocksJson.push(this.blocks[i].toJson());
    }

    // Save all of the links
    var linksJson = this.getLinksJson();
    
    var json = {
        blocks: blocksJson,
        links: linksJson,
        uuid: this.uuid
    };    
    return json;
};

Workflow.prototype.getLinksJson = function(){
    var linksJson = new Array();
    
    for(var i=0;i<this.blocks.length;i++){
        for(var j=0;j<this.blocks[i].getOutputCount();j++){
            for(var k=0;k<this.blocks[i].outputs[j].connections.length;k++){
                linksJson.push(this.blocks[i].outputs[j].connections[k].toJson());
            }
        }
    }
    return linksJson;
};

exports.Workflow = Workflow;

exports.newWorkflow = function() {
    return new Workflow();
};
